package build.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BuildController {
	
	
	
	@RequestMapping("/")
	public String  hello(Model model) {
		
		model.addAttribute("ms", "John Doe");
		return "hello";
	}

}
